@extends('layout.master')

@section('content')

    <div class="container">
        <div class="vacancy">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Title</span>
                <input type="text" id="title" class="form-control" placeholder="title" aria-describedby="basic-addon1">
            </div><br />
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Description</span>
                <textarea id="description" title="description" class="form-control custom-control" rows="3" style="resize:none"></textarea>
            </div><br />
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Email</span>
                <input type="text" id="email" class="form-control" placeholder="email" aria-describedby="basic-addon1">
            </div><br />
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-default" id="js-send-vacancy">Submit</button>
        </div>
    </div>

@endsection
