<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">

    <title>Holiday Pirates test</title>

</head>
<body>
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Holiday Pirates test</h1>
    </div>
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="/">Home</a></li>
        <li role="presentation"><a href="<?php echo url('/vacancy/add') ?>">Add Vacancy</a></li>
    </ul>
</div>

<div class="container">

    @yield('content')

    <hr>

    <footer>
        <p>&copy; 2016 </p>
    </footer>
</div>

<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vacancy.js') }}"></script>
</body>
</html>
