<?php
namespace App\Notification;

/**
 * Interface NotificationInterface
 * @package App\Notification
 */
interface NotificationInterface
{
    /**
     * @param array $data
     */
    public function send($data);
}
