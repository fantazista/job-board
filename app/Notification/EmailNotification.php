<?php

namespace App\Notification;

use Illuminate\Mail\Mailer;

class EmailNotification implements NotificationInterface
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * VacancyNotification constructor.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param array $data
     */
    public function send($data)
    {
        $emailData = array_get($data, 'email_data', array());
        $template = array_get($data, 'template', array());
        $recipientMail = array_get($data, 'recipient_mail', array());

        $this->mailer->send($template, $emailData, function ($message) use ($recipientMail) {
            $message->from('us@example.com', 'new vacancy');
            $message->to($recipientMail);
        });
    }
}
