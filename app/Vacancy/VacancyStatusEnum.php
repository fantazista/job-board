<?php

namespace App\Vacancy;

class VacancyStatusEnum
{
    const STATUS_APPROVE = 'approve';
    const STATUS_SPAM = 'spam';
}
