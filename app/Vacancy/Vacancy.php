<?php
namespace App\Vacancy;

use App\Notification\NotificationInterface;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Database\ConnectionInterface;

/**
 * Class Vacancy
 * @package App\Vacancy
 */
class Vacancy
{
    /**
     * @var ConnectionInterface
     */
    private $storage;

    /**
     * @var Mailer
     */
    private $notification;

    /**
     * Vacancy constructor.
     * @param NotificationInterface $notification
     * @param ConnectionInterface $storage
     */
    public function __construct(ConnectionInterface $storage, NotificationInterface $notification)
    {
        $this->storage = $storage;
        $this->notification = $notification;
    }

    /**
     * TODO Maybe more right do not write vacancy at db immediately.
     * Just after publish from email
     * But I don't clear understand that from test description
     *
     * @param array $data
     * @return bool
     */
    public function addVacancy(array $data)
    {
        // validate data

        // I can create second table with white list emails. But I thought is overhead for this task
        $vacancies = $this->storage ->select('select * from `vacancies` where `email` = ?', [$data['email']]);
        $status = $this->getStatus($vacancies);
        $data['id'] = $this->storage->table('vacancies')->insertGetId($data);

        if ($status == VacancyStatusEnum::STATUS_SPAM) {
            $this->sendToHr($data);
            $this->sendToPublisher(array_get($data, 'email', ''));
        }

        return true;
    }

    /**
     * @return array
     */
    public function getVacancies()
    {
        return $this->storage
            ->select('select * from `vacancies` where status = ?', [VacancyStatusEnum::STATUS_APPROVE]);
    }

    /**
     * @param int $id
     * @return int
     */
    public function publishVacancy($id)
    {
        return $this->storage->update('
                update `vacancies` set status = ? where id =?', [VacancyStatusEnum::STATUS_APPROVE, $id]
        );
    }

    /**
     * @param int $id
     * @return int
     */
    public function markAsSpam($id)
    {
        return $this->storage->update('
                update `vacancies` set status = ? where id =?', [VacancyStatusEnum::STATUS_SPAM, $id]
        );
    }

    /**
     * @param array $vacancies
     * @return string
     */
    private function getStatus($vacancies)
    {
        if ($vacancies) {
            return VacancyStatusEnum::STATUS_APPROVE;
        } else {
            return VacancyStatusEnum::STATUS_SPAM;
        }
    }

    /**
     * @param array $data
     */
    private function sendToHr($data)
    {
        $notificationData = array(
            'email_data' => $data,
            'template' => 'email.new-vacancy',
            'recipient_mail' => 'hr@example.com',
        );

        $this->notification->send($notificationData);
    }

    /**
     * @param string $publisherEmail
     */
    private function sendToPublisher($publisherEmail)
    {
        $notificationData = array(
            'template' => 'email.moderation.blade.php',
            'recipient_mail' => $publisherEmail,
        );

        $this->notification->send($notificationData);
    }
}