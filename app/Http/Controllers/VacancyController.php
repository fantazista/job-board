<?php

namespace App\Http\Controllers;

use App\Vacancy\Vacancy;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class VacancyController extends BaseController
{

    /**
     * @var Vacancy
     */
    private $vacancy;

    /**
     * VacancyController constructor.
     * @param Vacancy $vacancy
     */
    public function __construct(Vacancy $vacancy)
    {
        $this->vacancy = $vacancy;
    }

    /**
     * show vacancies
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showVacancies()
    {
        $vacancies = json_decode(json_encode($this->vacancy->getVacancies()), true);
        return view('index', ['vacancies' => $vacancies]);
    }

    /**
     * Form for vacancy
     */
    public function addVacancy()
    {
        return view('vacancy');
    }

    /**
     * Send vacancy or publish
     * @return bool
     */
    public function sendVacancy()
    {
        $data = array(
            'title' => (string) Input::get('title'),
            'description' => (string) Input::get('description'),
            'email' => (string) Input::get('email'),
        );

        return response()->json(['response' => $this->vacancy->addVacancy($data)]);
    }

    /**
     * Send vacancy or publish
     * @param Request $request
     */
    public function publishVacancy(Request $request)
    {
        $id = (int) $request->input('id');
        $this->vacancy->publishVacancy($id);
    }

    /**
     * mark vacancy as a spam
     * @param Request $request
     */
    public function markSpamVacancy(Request $request)
    {
        $id = (int) $request->input('id');
        $this->vacancy->markAsSpam($id);
    }
}
