<?php

namespace App\Providers;

use App\Vacancy\Vacancy;
use Illuminate\Support\ServiceProvider;

class VacancyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
       //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('vacancy', function($app)
        {
            return new Vacancy($this->app['db.connection'], $app['App\Notification\EmailNotification']);
        });
    }
}
