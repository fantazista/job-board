<?php

namespace App\Providers;

use App\Notification\EmailNotification;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Notification\NotificationInterface', 'App\Notification\EmailNotification', function($app) {
            return new EmailNotification($app['mailer']);
        });
    }
}
