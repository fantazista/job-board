<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'VacancyController@showVacancies');
Route::get('vacancy/add', 'VacancyController@addVacancy');
Route::post('vacancy/send', 'VacancyController@sendVacancy');
Route::get('vacancy/publish', 'VacancyController@publishVacancy');
Route::get('vacancy/spam', 'VacancyController@markSpamVacancy');
