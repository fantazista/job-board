$(document).ready(function() {

    $('#js-send-vacancy').on('click', sendVacancy);

    function sendVacancy() {
        var data = {
            title: $('#title').val(),
            description: $('#description').val(),
            email: $('#email').val(),
            _token: $('#_token').val()
        };

        $.post('/vacancy/send', data,
            function( data ) {
                if (data.response) {
                    alert('success');
                } else {
                    alert('error');
                }
            }
        );
    }
});
